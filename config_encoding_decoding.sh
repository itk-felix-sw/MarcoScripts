#! /bin/bash                                                                                                                                                                                                                                                                              
#in the comments the allowed options for the pixel F/W flavors are indicated                                                                                                                                  
LINK_MIN=0
LINK_STOP=0 # total channels / 2 was 2 before
LINK_MAX=11 # total channels / 2 
DEC_EGROUP_MIN=0
DEC_EGROUP_MAX=6
ENC_EGROUP_MIN=0
ENC_EGROUP_MAX=4 #was 3 before


flx-config -d $1 set DECODING_REVERSE_10B=0x1

for ((il = LINK_MIN ; il <= LINK_STOP ; il++)); do
  echo "Decoding Settings for Link: ${il}"
  il2=$(printf '%02d\n' $il)
  flx-config -d $1 set MINI_EGROUP_FROMHOST_${il2}_EC_ENABLE=0x1
  flx-config -d $1 set MINI_EGROUP_TOHOST_${il2}_EC_ENABLE=0x1
  flx-config -d $1 set MINI_EGROUP_FROMHOST_${il2}_IC_ENABLE=0x1
  flx-config -d $1 set MINI_EGROUP_TOHOST_${il2}_IC_ENABLE=0x1



  for ((ig = DEC_EGROUP_MIN ; ig <= DEC_EGROUP_MAX ; ig++)); do
      echo "***Decoding Egroup ${ig}***"
      
      flx-config -d $1 set DECODING_LINK${il2}_EGROUP${ig}_CTRL_EPATH_ENA=0x1      #0x1=Epath=0 (Data Frames); 0x2=Epath1 (Register Frames); 0x3=Epath0&1                      
      flx-config -d $1 set DECODING_LINK${il2}_EGROUP${ig}_CTRL_EPATH_WIDTH=0x4    #0x4=32b for all Epaths                                                                     
      flx-config -d $1 set DECODING_LINK${il2}_EGROUP${ig}_CTRL_PATH_ENCODING=0x33 #3=64b66b; 0x03 for Epath0; 0x30 for EPath1; 0x33 for Epath0&1                                                                                                                                         
  done

  for ((ig = ENC_EGROUP_MIN ; ig <= ENC_EGROUP_MAX ; ig++)); do
      echo "Encoding Egroup ${ig}"
      flx-config -d $1 set ENCODING_LINK${il2}_EGROUP${ig}_CTRL_EPATH_ENA=0x5            #0x1=Epath0; 0x4=Epath2; 0x5=Epath0&2                                                     
      flx-config -d $1 set ENCODING_LINK${il2}_EGROUP${ig}_CTRL_EPATH_WIDTH=0x1           #0x1=4b for all Epaths                                                                   
      flx-config -d $1 set ENCODING_LINK${il2}_EGROUP${ig}_CTRL_PATH_ENCODING=0x0404      #4=RD53 (4b per Epaths); 0x4 for Epath0; 0x0400 for Epath2; 0x0404 for Epath0&2                                                                                                                       

  done

done



for ((il = LINK_STOP+1 ; il <= LINK_MAX ; il++)); do
  echo "Disabling Settings for Link: ${il}"
  il2=$(printf '%02d\n' $il)
  flx-config -d $1 set MINI_EGROUP_FROMHOST_${il2}_EC_ENABLE=0x0
  flx-config -d $1 set MINI_EGROUP_TOHOST_${il2}_EC_ENABLE=0x0
  flx-config -d $1 set MINI_EGROUP_FROMHOST_${il2}_IC_ENABLE=0x0
  flx-config -d $1 set MINI_EGROUP_TOHOST_${il2}_IC_ENABLE=0x0


  for ((ig = DEC_EGROUP_MIN ; ig <= DEC_EGROUP_MAX ; ig++)); do
      echo "***Decoding Egroup ${ig}***"
      
      flx-config -d $1 set DECODING_LINK${il2}_EGROUP${ig}_CTRL_EPATH_ENA=0x0      #0x1=Epath=0 (Data Frames); 0x2=Epath1 (Register Frames); 0x3=Epath0&1                      
      flx-config -d $1 set DECODING_LINK${il2}_EGROUP${ig}_CTRL_EPATH_WIDTH=0x0    #0x4=32b for all Epaths                                                                     
      flx-config -d $1 set DECODING_LINK${il2}_EGROUP${ig}_CTRL_PATH_ENCODING=0x0 #3=64b66b; 0x03 for Epath0; 0x30 for EPath1; 0x33 for Epath0&1                                                                                                                                        #
  done

  for ((ig = ENC_EGROUP_MIN ; ig <= ENC_EGROUP_MAX ; ig++)); do
      echo "Encoding Egroup ${ig}"
      flx-config -d $1 set ENCODING_LINK${il2}_EGROUP${ig}_CTRL_EPATH_ENA=0x0            #0x1=Epath0; 0x4=Epath2; 0x5=Epath0&2                                                     
      flx-config -d $1 set ENCODING_LINK${il2}_EGROUP${ig}_CTRL_EPATH_WIDTH=0x0           #0x1=4b for all Epaths                                                                   
      flx-config -d $1 set ENCODING_LINK${il2}_EGROUP${ig}_CTRL_PATH_ENCODING=0x0      #4=RD53 (4b per Epaths); 0x4 for Epath0; 0x0400 for Epath2; 0x0404 for Epath0&2                                                                                                                  #   

  done

done
